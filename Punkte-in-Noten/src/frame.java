//Import der f�r die GUI ben�tigten Bibliotheken
import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import java.awt.TextField;
import java.awt.Button;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;
import javax.swing.event.ChangeListener;
import javax.swing.SpinnerModel;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.SystemColor;
import javax.swing.border.LineBorder;
public class frame extends JFrame {

	//Deklaration von Content Pane, der Tabelle sowie des Scroll Panes; werden sp�ter gef�llt
	private JPanel contentPane;
	private JTable table;
	private JScrollPane scrollPane;

	// Array mit den IHK-Noten. Das jeweilige Arrayfeld entspricht der Prozentanzahl. Schl�sselwort final entspricht const (Konstante)
		public static final double marksIHK[] = {6.0,6.0,6.0,6.0,6.0,6.0,5.9,5.9,5.9,5.9,5.9,5.9,5.8,5.8,5.8,5.8,5.8,5.7,5.7,5.7,5.7,5.7,5.7,5.6,5.6,5.6,5.6,5.6,5.6,5.5,5.4,5.4,5.3,5.3,5.2,5.2,5.1,5.1,5.0,5.0,5.0,4.9,4.9,4.8,4.8,4.7,4.7,4.6,4.6,4.5,4.4,4.4,4.3,4.3,4.2,4.1,4.1,4.0,4.0,3.9,3.9,3.8,3.7,3.7,3.6,3.6,3.5,3.4,3.3,3.3,3.2,3.1,3.1,3.0,2.9,2.9,2.8,2.7,2.7,2.6,2.5,2.4,2.3,2.2,2.1,2.0,2.0,1.9,1.8,1.7,1.6,1.5,1.4,1.4,1.3,1.3,1.2,1.2,1.1,1.1,1.0};
		
	
	/**
	 * Launch the application. - automatisch generiert vom Eclipse Windowsbuilder
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame frame = new frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Methode zum Berechnen der Prozente + IHK-Noten 
	//�bergeben werden: die maximale Punktanzahl + wieviel Prozent die maximalen Punkte entsprechen
	//Ausgegeben wird ein Array, was sp�ter in die Tabellenspalten geschrieben wird
	public static String[][] calculateMarks (int maxPoints, int totalPercentage) {
		int pointPercentage = 0;
		double markIHK = 0.0;
		String[][] output = new String[maxPoints+1][4];
		
		//Ausrechnen und Aufbereiten der Ergebnisse als Array f�r die Tabelle
		for (int point=maxPoints; point>=0; point--) {
			pointPercentage = (int) (((double)point/(double)maxPoints)*(double)totalPercentage);
			markIHK = marksIHK[pointPercentage];
			if (totalPercentage == 100) {
				output[maxPoints-point][0] = String.format("%d", point);				//Punkte
				output[maxPoints-point][1] = String.format("%d %%", pointPercentage);	//Prozente
				output[maxPoints-point][2] = String.format("%.1f", markIHK);			//IHK-Note
				output[maxPoints-point][3] = String.format("%.0f", markIHK);			//ganze Note
			}
			else {
				output[maxPoints-point][0] = String.format("%d", point);			//Punkte
				output[maxPoints-point][1] = String.format("%d %%", pointPercentage);	//Prozente
			}
		}
		return output; //R�ckgabe des Arrays
	}
	
	//Methode zum Anzeigen bzw. Aktualisieren der Tabelle
	//�bergeben werden: Die eingegebenen Werte f�r die Maximale Punktezahl und f�r die Prozente
	public void updateTable(int maxPoints, int totalPercentage) {
		String[][] res =  calculateMarks(maxPoints, totalPercentage); //Aufruf der Methode zum Berechnen und Zwischenspeicherung in res
		String[] columnNames = {"Punkte", "Prozente", "IHK-Note", "ganze Note"}; //Spaltennamen f�r die Tabelle
		
		table = new JTable(res, columnNames) {
			 @Override
			    public boolean isCellEditable(int row, int column) { //Editieren der Tabelle f�r alle Zellen deaktivieren
			        return false;
			    }
		};
		table.setRowSelectionAllowed(false); //Zeilenauswahl f�r die Tabelle deaktivieren
		
		scrollPane.setViewportView(table);	//Anzeigen der Tabelle im ScrollPane
		
	}
	
	//Methode zum Zusammenrechnen eines A- und B-Teils einer AP1 oder AP2
	//�bergeben werden: die erreichten Punkte im A-Teil, die erreichten Punkte im B-Teil, die erreichbaren Punkte im A-Teil, die erreichbaren Punkte im B-Teil, die prozentuale Gewichtung des A-Teils, die prozentuale Gewichtung des B-Teils
	public String[] calculatePart(int pointsA, int pointsB, int maxPointsA, int maxPointsB, int percentageA, int percentageB) {
		//Umrechnung der �bergebenen Prozente in Faktoren
		double calcPercentageA = (double)percentageA/(double)100;
		double calcPercentageB = (double)percentageB/(double)100;
		
		//Berechnung der gewichteten erreichten Prozente in Teil A und B
		double percentagePartA = (calcPercentageA*(double)pointsA/(double)maxPointsA)*(double)100;
		double percentagePartB = (calcPercentageB*(double)pointsB/(double)maxPointsB)*(double)100;
		
		//Zusammenrechnen zu einem Gesamtergebnis (max. 100%)
		double totalPercentage = percentagePartA + percentagePartB;
		//heraussuchen der entsprechenden IHK-Note aus dem Array
		double markIHK = marksIHK[(int)totalPercentage];
		
		//Initialisierung eines Arrays zum zwischenspeichern der Ergebnisse
		String[] res = new String[6];
		//Speichern der Ergebnisse im Array
		res[0] = String.format("%.2f", percentagePartA);
		res[1] = String.format("%.2f", percentagePartB);
		res[2] = String.format("%.2f", totalPercentage);
		res[3] = String.format("%.0f", totalPercentage);
		res[4] = String.format("%.1f", markIHK);
		res[5] = String.format("%.0f", markIHK);
		return res; //R�ckgabe der Ergebnisse als Array
	}
	
	//Methode zum Zusammenrechnen der einzelnen AP2-Teile
	//�bergaben werden: Ein Array mit den Gewichtungen der einzelnen Teile, ein Array mit den erreichten Prozenten der einzelnen Teile (ungewichtet)
	public int calculateTotal(int[]w, int percentage[]) {
		String[] res = new String[6]; //Array zum Speichern der Ergebnisse
		double[] temp = new double[w.length]; //Array f�r Zwischenergebnisse
		int totalPercentage = 0; //Variable f�r das Endergebnis
		
		// For-Schleife f�r alle Pr�fungsteile
		for (int i=0; i<w.length; i++) {
			//Prozente in Faktoren umwandeln
			temp[i] = (double)w[i]/(double)100;
			//gewichtete erreichte Prozente des jeweiligen Teils
			temp[i] = (double)temp[i]*(double)percentage[i];
			//hinzurechnen des Teilergebnisses zum Gesamtergebnis
			totalPercentage += temp[i];
		}
		
		return totalPercentage; //R�ckgabe der gesamten erreichten Prozente
	}
	
	//Methode f�r das Zusammenbauen der Ausgabe f�r AP1
	public String buildOutString (String title, String[] res, JSpinner spPartA, JSpinner spPartB) {
		String out = String.format("%s \n\nrichtig gel�ste gebundene Aufgaben \t%d:0,5 = %s %%\n"
				+ "err. Punkte ungebundene Aufgaben \t%d:1,6 = %s %%\n"
				+ "Ergebnis in Punkten (max.100) \t%s %% \n \t\t\t%s %%\n"
				+ "IHK-Note:  %s\nganze Note:  %s", title, (int)spPartA.getValue(), res[0], (int)spPartB.getValue(), res[1], res[2], res[3], res[4], res[5]);
				return out;
	}
	
	/**
	 * Create the frame.
	 */
	public frame() {
		setResizable(false);
		
		//maximal Erreichbare Punkte in den einzelnen Pr�fungsteilen (f�r eine Anpassung m�ssen diese nur hie ge�ndert werden)
		final int pointsAP1PartA = 20;
		final int pointsAP1PartB = 80;
		final int pointsAP2_SEPartA = 25;
		final int pointsAP2_SEPartB = 80;
		final int pointsAP2_WisoPartA = 15;
		final int pointsAP2_WisoPartB = 50;
		final int pointsAP2_FusPartA = 25;
		final int pointsAP2_FusPartB = 80;
		
		// Hier folgen die GUI-Elemente welche vom Eclipse WindowBuilder generiert wurden
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 815, 529);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.controlHighlight);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTotalPercentage = new JLabel("Prozente");
		lblTotalPercentage.setBounds(160, 48, 73, 20);
		contentPane.add(lblTotalPercentage);
		
		JLabel lblMaxPoints = new JLabel("Gesamtpunktzahl");
		lblMaxPoints.setBounds(25, 48, 131, 20);
		contentPane.add(lblMaxPoints);
		
		
		// 0 als Startwert, von 0 bis ..., Schrittweite 1
		SpinnerNumberModel modelMaxPoints = new SpinnerNumberModel(0, 0, null, 1);
		JSpinner spMaxPoints = new JSpinner(modelMaxPoints);
		spMaxPoints.setBounds(65, 75, 60, 25);
		contentPane.add(spMaxPoints);
		
		
		// 100 als Startwert, von 0 bis 100, Schrittweite 1
		SpinnerNumberModel modelTotalPercentage = new SpinnerNumberModel(100, 0, 100, 10);  
		JSpinner spTotalPercentage = new JSpinner(modelTotalPercentage);
		spTotalPercentage.setBounds(160, 75, 58, 26);
		contentPane.add(spTotalPercentage);
		
		//Tabelle aktualisieren wenn Punkte ge�ndert werden
		spMaxPoints.addChangeListener(new ChangeListener() {
			 @Override
	            public void stateChanged(ChangeEvent e) {
				 updateTable((int)spMaxPoints.getValue(), (int)spTotalPercentage.getValue());
		}
	});
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(399, 16, 379, 453);
		contentPane.add(scrollPane);
		
		
		
		Button btnCalc = new Button("Berechnen");
		//Event Listener, der wenn der Button gedr�ckt wird, die Tabelle aktualisiert
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateTable((int)spMaxPoints.getValue(), (int)spTotalPercentage.getValue());
			}
		});
		btnCalc.setBounds(270, 73, 91, 27);
		contentPane.add(btnCalc);	
		
		JLabel lblTest = new JLabel("Test");
		lblTest.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTest.setBounds(15, 15, 69, 20);
		contentPane.add(lblTest);
		
		JLabel lblAP1 = new JLabel("AP1");
		lblAP1.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAP1.setBounds(15, 135, 69, 20);
		contentPane.add(lblAP1);
		
		// 0 als Startwert, von 0 bis 20, Schrittweite 1
		SpinnerNumberModel modelAP1_PartA = new SpinnerNumberModel(0, 0, pointsAP1PartA, 1);
		JSpinner spAP1_PartA = new JSpinner(modelAP1_PartA);
		spAP1_PartA.setBounds(65, 180, 58, 26);
		contentPane.add(spAP1_PartA);
		
		// 0 als Startwert, von 0 bis ..., Schrittweite 1
		SpinnerNumberModel modelAP1_PartB = new SpinnerNumberModel(0, 0, pointsAP1PartB, 1);
		JSpinner spAP1_PartB = new JSpinner(modelAP1_PartB);
		spAP1_PartB.setBounds(160, 180, 58, 26);
		contentPane.add(spAP1_PartB);
		
		JLabel lblAP1_PartA = new JLabel("Teil A");
		lblAP1_PartA.setBounds(65, 157, 131, 20);
		contentPane.add(lblAP1_PartA);
		
		JLabel lblAP1_PartB = new JLabel("Teil B");
		lblAP1_PartB.setBounds(160, 157, 131, 20);
		contentPane.add(lblAP1_PartB);
		
		JLabel lblAP2 = new JLabel("AP2");
		lblAP2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblAP2.setBounds(15, 225, 69, 20);
		contentPane.add(lblAP2);
		
		JLabel lblAP2_SE_PartA = new JLabel("Teil A");
		lblAP2_SE_PartA.setBounds(65, 245, 131, 20);
		contentPane.add(lblAP2_SE_PartA);
		
		JLabel lblAP2_SE_PartB = new JLabel("Teil B");
		lblAP2_SE_PartB.setBounds(160, 245, 131, 20);
		contentPane.add(lblAP2_SE_PartB);
		
		// 0 als Startwert, von 0 bis 20, Schrittweite 1
		SpinnerNumberModel modelAP2_SE_PartA = new SpinnerNumberModel(0, 0, pointsAP2_SEPartA, 1);
		JSpinner spAP2_SE_PartA = new JSpinner(modelAP2_SE_PartA);
		spAP2_SE_PartA.setBounds(65, 270, 58, 26);
		contentPane.add(spAP2_SE_PartA);
		
		// 0 als Startwert, von 0 bis 20, Schrittweite 1
		SpinnerNumberModel modelAP2_SE_PartB = new SpinnerNumberModel(0, 0, pointsAP2_SEPartB, 1);
		JSpinner spAP2_SE_PartB = new JSpinner(modelAP2_SE_PartB);
		spAP2_SE_PartB.setBounds(160, 270, 58, 26);
		contentPane.add(spAP2_SE_PartB);
		
		JLabel lblAP2_SE = new JLabel("SE");
		lblAP2_SE.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAP2_SE.setBounds(15, 270, 69, 26);
		contentPane.add(lblAP2_SE);
		
		JLabel lblAP2_Fus = new JLabel("Fus");
		lblAP2_Fus.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAP2_Fus.setBounds(15, 340, 69, 25);
		contentPane.add(lblAP2_Fus);
		
		JLabel lblAP2_Fus_PartA = new JLabel("Teil A");
		lblAP2_Fus_PartA.setBounds(65, 317, 131, 20);
		contentPane.add(lblAP2_Fus_PartA);
		
		
		// 0 als Startwert, von 0 bis 20, Schrittweite 1
		SpinnerNumberModel modelAP2_Fus_PartA = new SpinnerNumberModel(0, 0, pointsAP2_FusPartA, 1);
		JSpinner spAP2_Fus_PartA = new JSpinner(modelAP2_Fus_PartA);
		spAP2_Fus_PartA.setBounds(65, 340, 58, 26);
		contentPane.add(spAP2_Fus_PartA);
		
		JLabel lblAP2_Fus_PartB = new JLabel("Teil B");
		lblAP2_Fus_PartB.setBounds(160, 315, 63, 20);
		contentPane.add(lblAP2_Fus_PartB);
		
		// 0 als Startwert, von 0 bis 20, Schrittweite 1
		SpinnerNumberModel modelAP2_Fus_PartB = new SpinnerNumberModel(0, 0, pointsAP2_FusPartB, 1);
		JSpinner spAP2_Fus_PartB = new JSpinner(modelAP2_Fus_PartB);
		spAP2_Fus_PartB.setBounds(160, 340, 58, 26);
		contentPane.add(spAP2_Fus_PartB);
		
		JLabel lblAP2_Wiso = new JLabel("Wiso");
		lblAP2_Wiso.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblAP2_Wiso.setBounds(15, 410, 69, 28);
		contentPane.add(lblAP2_Wiso);
		
		JLabel lblAP2_Wiso_PartA = new JLabel("Teil A");
		lblAP2_Wiso_PartA.setBounds(65, 389, 131, 20);
		contentPane.add(lblAP2_Wiso_PartA);
		
		
		// 0 als Startwert, von 0 bis 20, Schrittweite 1
		SpinnerNumberModel modelAP2_Wiso_PartA = new SpinnerNumberModel(0, 0, pointsAP2_WisoPartA, 1);
		JSpinner spAP2_Wiso_PartA = new JSpinner(modelAP2_Wiso_PartA);
		spAP2_Wiso_PartA.setBounds(65, 410, 58, 26);
		contentPane.add(spAP2_Wiso_PartA);
		
		JLabel lblAP2_Wiso_PartB = new JLabel("Teil B");
		lblAP2_Wiso_PartB.setBounds(160, 387, 131, 20);
		contentPane.add(lblAP2_Wiso_PartB);
		
		
		// 0 als Startwert, von 0 bis 20, Schrittweite 1
		SpinnerNumberModel modelAP2_Wiso_PartB = new SpinnerNumberModel(0, 0, pointsAP2_WisoPartB, 1);
		JSpinner spAP2_Wiso_PartB = new JSpinner(modelAP2_Wiso_PartB);
		spAP2_Wiso_PartB.setBounds(160, 410, 58, 26);
		contentPane.add(spAP2_Wiso_PartB);
		
		Button btnCalcAP1 = new Button("Berechnen");
		//Event Listener, der wenn der Button gedr�ckt wird, die Ergebnisse ausgibt
		btnCalcAP1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] resWiso = calculatePart((int)spAP1_PartA.getValue(), (int)spAP1_PartB.getValue(), pointsAP1PartA, pointsAP1PartB, 50, 50);
				JTextPane txtpnGesamt = new JTextPane();
				txtpnGesamt.setText(buildOutString("AP1", resWiso, spAP1_PartA, spAP1_PartB));
				txtpnGesamt.setEditable(false);
				txtpnGesamt.setBackground(getBackground());
				scrollPane.setViewportView(txtpnGesamt);
				
			}
		});
		btnCalcAP1.setBounds(270, 183, 91, 27);
		contentPane.add(btnCalcAP1);
		
		Button btnCalcAP2 = new Button("Berechnen");
		//Event Listener, der wenn der Button gedr�ckt wird, die Ergebnisse ausgibt
		btnCalcAP2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] resSE = calculatePart((int)spAP2_SE_PartA.getValue(), (int)spAP2_SE_PartB.getValue(), pointsAP2_SEPartA, pointsAP2_SEPartB, 50, 50);
				String[] resFus = calculatePart((int)spAP2_Fus_PartA.getValue(), (int)spAP2_Fus_PartB.getValue(), pointsAP2_FusPartA, pointsAP2_FusPartB, 50, 50);
				String[] resWiso = calculatePart((int)spAP2_Wiso_PartA.getValue(), (int)spAP2_Wiso_PartB.getValue(), pointsAP2_WisoPartA, pointsAP2_WisoPartB, 40, 60);
				int[] partsPercentage = {40, 40, 20};
				int[] resParts = {Integer.valueOf(resSE[3]), Integer.valueOf(resFus[3]), Integer.valueOf(resWiso[3])};
				int totalPercentage = calculateTotal(partsPercentage, resParts);
				//Zusammenbau der Ausgabetexte; besser w�re hier Auslagerung in Methode, aber zeutlich nicht mehr geschafft
				String textSE = "Systementwurf\n=============\n"
						+ "richtig gel�ste gebundene Aufgaben\t" + spAP2_SE_PartA.getValue() + ":0,5 = " + resSE[0] + " %\n"
						+ "err. Punkte ungebundene Aufgaben\t" + spAP2_SE_PartB.getValue() + ":1,6 = " + resSE[1] + "\n"
						+ "Ergebnis in Punkten (max.100) \t" + resSE[2] + " %\n \t\t\t" + resSE[3] + " %\n"
						+ "IHK-Note:  " + resSE[4] + "\nganze Note:  " + resSE[5];
				String textFus = "Funktions- und Systemanalyse\n=============\n"
						+ "richtig gel�ste gebundene Aufgaben\t" + spAP2_Fus_PartA.getValue() + ":0,5 = " + resFus[0] + " %\n"
						+ "err. Punkte ungebundene Aufgaben\t" + spAP2_Fus_PartB.getValue() + ":1,6 = " + resFus[1] + " %\n"
						+ "Ergebnis in Punkten (max.100) \t" + resFus[2] + " %\n \t\t\t" + resFus[3] + " %\n"
						+ "IHK-Note:  " + resFus[4] + "\nganze Note:  " + resFus[5];
				String textWiso = "Wirtschafts- und Sozialkunde\n=============\n"
						+ "richtig gel�ste gebundene Aufgaben\t" + spAP2_Wiso_PartA.getValue() + ":0,375 = " + resWiso[0] + " %\n"
						+ "err. Punkte ungebundene Aufgaben\t" + spAP2_Wiso_PartB.getValue() + "*1,2 = " + resWiso[1] + " %\n"
						+ "Ergebnis in Punkten (max.100) \t" + resWiso[2] + " %\n \t\t\t" + resWiso[3] + " %\n"
						+ "IHK-Note:  " + resWiso[4] + "\nganze Note:  " + resWiso[5];
				String textGes = String.format("Gesamtergebnis\n=============\n%d %%\nIHK-Note: %.1f\nganze Note: %.0f", totalPercentage, marksIHK[totalPercentage], marksIHK[totalPercentage]);
				String output = "AP2\n\n" + textGes + "\n\n" + textSE + "\n\n" + textFus + "\n\n" + textWiso + "\n";
				JTextPane txtpnGesamt = new JTextPane();
				txtpnGesamt.setText(output);
				txtpnGesamt.setEditable(false);
				txtpnGesamt.setBackground(getBackground());
				scrollPane.setViewportView(txtpnGesamt);
			}
		});
		btnCalcAP2.setBounds(270, 340, 91, 27);
		contentPane.add(btnCalcAP2);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.control);
		panel.setBounds(0, 0, 379, 482);
		contentPane.add(panel);
		
	}
}
