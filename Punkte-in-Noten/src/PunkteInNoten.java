import java.util.Scanner;
public class PunkteInNoten {
	
	// Array mit den IHK-Noten als Konstante. Das jeweilige Arrayfeld entspricht der Prozentanzahl 
	public static final double marksIHK[] = {6.0,6.0,6.0,6.0,6.0,6.0,5.9,5.9,5.9,5.9,5.9,5.9,5.8,5.8,5.8,5.8,5.8,5.7,5.7,5.7,5.7,5.7,5.7,5.6,5.6,5.6,5.6,5.6,5.6,5.5,5.4,5.4,5.3,5.3,5.2,5.2,5.1,5.1,5.0,5.0,5.0,4.9,4.9,4.8,4.8,4.7,4.7,4.6,4.6,4.5,4.4,4.4,4.3,4.3,4.2,4.1,4.1,4.0,4.0,3.9,3.9,3.8,3.7,3.7,3.6,3.6,3.5,3.4,3.3,3.3,3.2,3.1,3.1,3.0,2.9,2.9,2.8,2.7,2.7,2.6,2.5,2.4,2.3,2.2,2.1,2.0,2.0,1.9,1.8,1.7,1.6,1.5,1.4,1.4,1.3,1.3,1.2,1.2,1.1,1.1,1.0};
	
	public static void main(String[] args) {
		int pointPercentage = 0;
		double markIHK = 0.0;
		// Scanner f�r Tastatureingaben
		Scanner keyboard = new Scanner (System.in);
		
		//Benutzereingabe
		System.out.print("maximale Punktanzahl: ");
		int maxPoints = keyboard.nextInt();
		System.out.print("Wievielen Prozenten sollen die Punkte entsprechen?: ");
		int totalPercentage = keyboard.nextInt();
		
		//For-Schleife f�r die Anzahl der Punkte
		for (int point=maxPoints; point>=0; point--) {
			pointPercentage = (int) (((double)point/(double)maxPoints)*(double)totalPercentage);
			markIHK = marksIHK[pointPercentage];
			if (totalPercentage == 100) 
				System.out.printf("%d Punkte  %d%% -> IHK-Note %.1f -> ganze Note %.0f \n", point, pointPercentage, markIHK, markIHK);
			else
				System.out.printf("%d Punkte  %d%%\n", point, pointPercentage);
		}
	}
	
	public static String[][] calculateMarks (int maxPoints, int totalPercentage) {
		int pointPercentage = 0;
		double markIHK = 0.0;
		String[][] output = new String[maxPoints+1][4];
		
		for (int point=maxPoints; point>=0; point--) {
			pointPercentage = (int) (((double)point/(double)maxPoints)*(double)totalPercentage);
			markIHK = marksIHK[pointPercentage];
			if (totalPercentage == 100) {
				output[maxPoints-point][0] = Integer.toString(point);
				output[maxPoints-point][1] = Integer.toString(pointPercentage) + " %";
				output[maxPoints-point][2] = Double.toString(markIHK);
				output[maxPoints-point][3] = Integer.toString((int)markIHK);
			}
			else {
				output[maxPoints-point][0] = Integer.toString(point);
				output[maxPoints-point][1] = Integer.toString(pointPercentage) + " %";
			}
		}
		return output;
	}

}
